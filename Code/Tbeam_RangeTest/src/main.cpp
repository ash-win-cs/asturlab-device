#include <Arduino.h>
#include <SPI.h>
#include <LoRa.h>

#define SCK 5   // GPIO5  -- SX1278's SCK
#define MISO 19 // GPIO19 -- SX1278's MISnO
#define MOSI 27 // GPIO27 -- SX1278's MOSI
#define SS 18   // GPIO18 -- SX1278's CS
#define RST 14  // GPIO14 -- SX1278's RESET
#define DI0 26  // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND 868E6

#define BUTTON 38
#define NOTIF_LED 4

unsigned int counter = 0;

void setup()
{
  pinMode(NOTIF_LED, OUTPUT);
  digitalWrite(NOTIF_LED,HIGH);

  Serial.begin(115200);
  while (!Serial)
    ;
  Serial.println();
  Serial.println("LoRa Sender Test");

  SPI.begin(SCK, MISO, MOSI, SS);
  LoRa.setPins(SS, RST, DI0);
  if (!LoRa.begin(868E6))
  {
    Serial.println("Starting LoRa failed!");
    while (1)
      ;
  }
  //LoRa.onReceive(cbk);
  //  LoRa.receive();
  Serial.println("init ok");
  pinMode(BUTTON, INPUT);

  delay(1500);
}

void loop()
{
  // send packet
  if (!digitalRead(BUTTON))
  {
    LoRa.beginPacket();
    LoRa.print("hello ");
    LoRa.print(counter);
    LoRa.endPacket();

    counter++;
    Serial.printf("No. of packet send = %d\n", counter);
    digitalWrite(NOTIF_LED, LOW); // turn the LED on (HIGH is the voltage level)
    delay(500);                   // wait for a second
    digitalWrite(NOTIF_LED, HIGH);
  }
}
