#include <Arduino.h>

#include <SPI.h>
#include <LoRa.h>
#include <TinyGPS++.h>
#include <axp20x.h> //Power Controller IC
#include <string.h>
#include <WiFi.h>

#include "bt_init.h"
#include "spiInit.h"  //SPI Pin Initialisations

TinyGPSPlus gps;
HardwareSerial Serial_GPS(1);
AXP20X_Class axp;


#define SERVICE_UUID "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID_USR_MSG "beb5483e-36e1-4688-b7f5-ea07361b26a8" //To transmit user messages
#define CHARACTERISTIC_UUID_PARAMS "beb5483e-36e1-4688-b7f5-ea07361b26a9"  //To transmit BW,SF,CR
#define CHARACTERISTIC_UUID_RSSI "0972EF8C-7613-4075-AD52-756F33D4DA91"    //To display RSSI
#define CHARACTERISTIC_UUID_MSG "0972EF8C-7613-4075-AD52-756F33D4DA92"     //To display Msg received
#define CHARACTERISTIC_UUID_BAT "0972EF8C-7613-4075-AD52-756F33D4DA93"     //Display battery percentage

BLECharacteristic *characteristicTX;
BLECharacteristic *characteristicTX2;
BLECharacteristic *characteristicTX3;

hw_timer_t *timer = NULL; // create a hardware timer

/****** Global Variables ******/
bool deviceConnected = false, pmu_irq = false, ble_flag = false, param_flag = false;
char rxv[256] = {};
float temp, vbat;
unsigned int counter = 0, params;
double mylong = 0, mylat = 0, oldlong = 0, oldlat = 0;
volatile byte led_state = LOW;
volatile int blink_count;

/******************************** 
 * Function Declaration
 ********************************/
void blink(int);
void blink_nonblocking(int);
static void smartDelay(unsigned long);
void sleepPMU();
double findDistance(double, double);
void setTxParams(long, long, int);
void IRAM_ATTR onTimer();


/****** BLE Class USR Msg ******/
class CharacteristicCallbacks : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *characteristic)
  {
    std::string rxValue = characteristic->getValue();
    if (rxValue.length() > 0)
    {

      for (int i = 0; i < rxValue.length(); i++)
      {
        Serial.print(rxValue[i]);
        rxv[i] = (char)rxValue[i];
      }
      Serial.println();
      ble_flag = true;
    }
  } //onWrite
};

/****** BLE Class Params ******/
class CharacteristicCallbacks2 : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *characteristic)
  {
    std::string param = characteristic->getValue();
    if (param.length() > 0 && param.length() < 2)
    {
      params = param[0];
      param_flag = true;
    }
  } //onWrite
};

class ServerCallbacks : public BLEServerCallbacks
{
  void onConnect(BLEServer *pServer)
  {
    blink_nonblocking(2);
    deviceConnected = true;
  };

  void onDisconnect(BLEServer *pServer)
  {
    blink_nonblocking(1);
    deviceConnected = false;
  }
};


/***********************************
 *  Setup
 ***********************************/
void setup()
{
  Serial.begin(115200);
  Wire.begin(21, 22);
  // Initializing AXP192
  if (!axp.begin(Wire, AXP192_SLAVE_ADDRESS))
  {
    Serial.println("AXP192 Begin PASS");
  }
  else
  {
    Serial.println("AXP192 Begin FAIL");
  }

  // Power on AXP192 outputs
  axp.setPowerOutPut(AXP192_LDO2, AXP202_ON);
  axp.setPowerOutPut(AXP192_LDO3, AXP202_ON);
  axp.setPowerOutPut(AXP192_DCDC2, AXP202_ON);
  axp.setPowerOutPut(AXP192_EXTEN, AXP202_ON);
  axp.setPowerOutPut(AXP192_DCDC1, AXP202_ON);

  Serial_GPS.begin(9600, SERIAL_8N1, 34, 12); //17-TX 18-RX

  pinMode(NOTIF_LED, OUTPUT);
  pinMode(BUTTON, INPUT);
  digitalWrite(NOTIF_LED, HIGH);
  while (!Serial)
    ;

  Serial.println("LoRa Receiver");
  SPI.begin(SCK, MISO, MOSI, SS);
  LoRa.setPins(SS, RST, DI0);
  if (!LoRa.begin(868E6))
  {
    Serial.println("Starting LoRa failed!");
    while (1)
      ;
  }
  LoRa.setTxPower(20);

  //Setting unique name based on MAC address
  String dev_name = "Asturlab-dev:" + WiFi.macAddress().substring(12);
  char temp_str[dev_name.length() + 1];
  dev_name.toCharArray(temp_str, dev_name.length() + 1);
  BLEDevice::init(temp_str);                      // Create the BLE Device
  BLEServer *pServer = BLEDevice::createServer(); // Create the BLE Server
  pServer->setCallbacks(new ServerCallbacks());   //Set callback()

  BLEService *pService = pServer->createService(SERVICE_UUID); // Create the BLE Service
  /*** Chara to Receive Msg from user ***/
  BLECharacteristic *CharacteristicRX = pService->createCharacteristic(
      CHARACTERISTIC_UUID_USR_MSG,
      BLECharacteristic::PROPERTY_READ |
          BLECharacteristic::PROPERTY_WRITE);
  CharacteristicRX->setCallbacks(new CharacteristicCallbacks());
  /*** Chara to Receive Parameters from user ***/
  BLECharacteristic *CharacteristicRX2 = pService->createCharacteristic(
      CHARACTERISTIC_UUID_PARAMS,
      BLECharacteristic::PROPERTY_READ |
          BLECharacteristic::PROPERTY_WRITE);
  CharacteristicRX2->setCallbacks(new CharacteristicCallbacks2());

  /*** Chara to Transmit RSSI ***/
  characteristicTX = pService->createCharacteristic(
      CHARACTERISTIC_UUID_RSSI,
      //BLECharacteristic::PROPERTY_NOTIFY |
      BLECharacteristic::PROPERTY_READ |
          BLECharacteristic::PROPERTY_WRITE);

  characteristicTX->addDescriptor(new BLE2902());
  characteristicTX->setValue("0");

  /*** Chara to Transmt LoRa Message ***/
  characteristicTX2 = pService->createCharacteristic(
      CHARACTERISTIC_UUID_MSG,
      //BLECharacteristic::PROPERTY_NOTIFY |
      BLECharacteristic::PROPERTY_READ |
          BLECharacteristic::PROPERTY_WRITE);

  characteristicTX2->addDescriptor(new BLE2902());

  /*** Chara to Transmit Battery percentage ***/
  characteristicTX3 = pService->createCharacteristic(
      CHARACTERISTIC_UUID_BAT,
      //BLECharacteristic::PROPERTY_NOTIFY |
      BLECharacteristic::PROPERTY_READ |
          BLECharacteristic::PROPERTY_WRITE);
  characteristicTX3->addDescriptor(new BLE2902());

  pService->start(); // Start the service
  // BLEAdvertising *pAdvertising = pServer->getAdvertising();  // this still is working for backward compatibility
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06); // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  BLEDevice::startAdvertising();
  Serial.println("Characteristic defined! Now you can read it in your phone!");
  //char str[3];
  //sprintf(str, "%02X", (int)ble_addrs);

  // Register the PMU interrupt pin, it will be triggered on the falling edge
  pinMode(IRQ, INPUT);
  attachInterrupt(
      IRQ, []
      { pmu_irq = true; },
      FALLING);

  // Before using IRQ, remember to clear the IRQ status register
  axp.clearIRQ();

  // Turn on the key to press the interrupt function
  axp.enableIRQ(AXP202_PEK_SHORTPRESS_IRQ, true);

  /*******  Timer *********/
  timer = timerBegin(0, 80, true);

  /* Attach onTimer function to our timer */
  timerAttachInterrupt(timer, &onTimer, true);

  /* Set alarm to call onTimer function every second 1 tick is 1us
  => 1 second is 1000000us */
  /* Repeat the alarm (third parameter) */
  timerAlarmWrite(timer, 100000, true);
}

/************************************* 
 * Loop 
 *************************************/
void loop()
{
  /************************************
   * Local variables
   * **********************************/
  char rssi[8], msg[256] = {};
  int packetSize = LoRa.parsePacket();

  /************************************
   * Code body
   * **********************************/
  // if(some LoRa packets are received)
  if (packetSize)
  {
    // read packet
    while (LoRa.available())
    {
      for (int i = 0; i < packetSize; i++)
        msg[i] = (char)LoRa.read();
    }
    // Read my GPS coordinates
    mylat = gps.location.lat();
    mylong = gps.location.lng();

    Serial.println("**********************");
    temp = axp.getTemp();
    vbat = axp.getBattVoltage();
    Serial.printf("\nTemperature = %f\nVBat = %f\n", temp, vbat);

    smartDelay(1000);
    // print packet & RSSI of packet
    Serial.printf("Received packet '%s' \nwith RSSI %d\n", msg, LoRa.packetRssi());
    dtostrf(LoRa.packetRssi(), 2, 0, rssi); // float_val, min_width, digits_after_decimal, char_buffer

    if (deviceConnected) //If BLE device is connected
    {
      characteristicTX->setValue(rssi); //RSSI
      characteristicTX->notify();

      characteristicTX2->setValue(msg); //Message
      characteristicTX2->notify();

      char buffer[64];
      snprintf(buffer, sizeof(buffer), "%f", vbat);
      characteristicTX3->setValue(buffer); //Battery in mV
      characteristicTX3->notify();
    } /*if (deviceConnected)*/

    blink_nonblocking(abs(LoRa.packetRssi() / 10));

    //No GPS data, Do something if you want
    if (millis() > 5000 && gps.charsProcessed() < 10)
      Serial.println(F("No GPS data received: check wiring"));
  } /*if (packetSize)*/

  // If PWR button pressed
  if (pmu_irq)
  {
    pmu_irq = false;
    axp.readIRQ();
    // When the PWR button is pressed briefly, the PMU is set to sleep
    if (axp.isPEKShortPressIRQ())
    {
      // Clear all interrupt status before going to sleep
      axp.clearIRQ();
      // Set PMU to sleep
      sleepPMU();
    }
  } /*if (pmu_irq)*/

  // if parameter setting changed
  if (param_flag)
  {
    blink_nonblocking(1);
    switch (params)
    {
    case '0':
      Serial.println("Short");
      setTxParams(7, 500e+3, 5); //BW = 500 kHz,CR = 4/5
      break;
    case '1':
      Serial.println("medium");
      setTxParams(7, 125e+3, 5);
      break;
    case '2':
      Serial.println("Long");
      setTxParams(7, 31.25e+3, 8);
      break;
    case '3':
      Serial.println("Long2");
      setTxParams(7, 125e+3, 8);
      break;
    case 'x': //Under construction
      Serial.println("Print My Params");
      break;

    default:
      Serial.println("Unknown Param");
      break;
    } /*switch params*/
    param_flag = false;
  } /*if param_flag*/

  // send packet
  if (!digitalRead(BUTTON) || ble_flag)
  {
    LoRa.beginPacket();
    blink_nonblocking(1);
    if (ble_flag)
    {
      if (mylat != gps.location.lat() && mylong != gps.location.lng()) // Do not send cordinates if no change in position
      {
        char charray1[sizeof(mylat) + 1], charray2[sizeof(mylong) + 1];

        mylat = gps.location.lat();
        mylong = gps.location.lng();
        sprintf(charray1, "%f", mylat);
        sprintf(charray2, "%f", mylong);
        String s1 = String(rxv) + " " + charray1 + "," + charray2;
        printf(s1.c_str());
        LoRa.print(s1.c_str());
      }
      else
        LoRa.print(rxv);
    }
    else if (!digitalRead(BUTTON))
    {
      blink_nonblocking(1);
      LoRa.print("hello ");
      LoRa.print(counter);
      LoRa.print(",");
      LoRa.print(gps.location.lat());
      LoRa.print(",");
      LoRa.print(gps.location.lng());
    }

    LoRa.endPacket();
    Serial.println(counter);
    delay(400); //For Button debouncing
    ble_flag = false;
    counter++;
  } /*send packet*/
} /*void loop ()*/


/****** Function Definitions ******/
void blink(int n)
{
  if (n == 0)
    n++;
  for (int i = 0; i < n; i++)
  {
    digitalWrite(NOTIF_LED, LOW);
    delay(1000 / n);
    digitalWrite(NOTIF_LED, HIGH);
    delay(1000 / n);
  }
}

static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    while (Serial_GPS.available())
      gps.encode(Serial_GPS.read());
  } while (millis() - start < ms);
}

void sleepPMU()
{
  int ret;
  // PEK or GPIO edge wake-up function enable setting in Sleep mode
  do
  {
    // In order to ensure that it is set correctly,
    // the loop waits for it to return the correct return value
    Serial.println("Set PMU in sleep mode");
    ret = axp.setSleep();
    delay(500);
  } while (ret != AXP_PASS);

  // Turn off all power channels, only use PEK or AXP GPIO to wake up

  // After setting AXP202/AXP192 to sleep,
  // it will start to record the status of the power channel that was turned off after setting,
  // it will restore the previously set state after PEK button or GPIO wake up

  blink(3);
  // Turn off all AXP192 power channels
  ret = axp.setPowerOutPut(AXP192_LDO2, AXP202_OFF);
  Serial.printf("Set Power AXP192_LDO2:%s\n", ret == AXP_PASS ? "OK" : "FAIL");

  ret = axp.setPowerOutPut(AXP192_LDO3, AXP202_OFF);
  Serial.printf("Set Power AXP192_LDO3:%s\n", ret == AXP_PASS ? "OK" : "FAIL");

  ret = axp.setPowerOutPut(AXP192_DCDC1, AXP202_OFF);
  Serial.printf("Set Power AXP192_DCDC1:%s\n", ret == AXP_PASS ? "OK" : "FAIL");

  ret = axp.setPowerOutPut(AXP192_DCDC2, AXP202_OFF);
  Serial.printf("Set Power AXP192_DCDC2:%s\n", ret == AXP_PASS ? "OK" : "FAIL");

  ret = axp.setPowerOutPut(AXP192_EXTEN, AXP202_OFF);
  Serial.printf("Set Power AXP192_EXTEN:%s\n", ret == AXP_PASS ? "OK" : "FAIL");

  Serial.flush();
  delay(1000);

  // Tbeam v1.0 uses DC3 as the MCU power channel, turning it off as the last
  ret = axp.setPowerOutPut(AXP192_DCDC3, AXP202_OFF);
  Serial.printf("Set Power AXP192_DCDC3:%s\n", ret == AXP_PASS ? "OK" : "FAIL");

  // Turn off all AXP202 power channels
  // axp.setPowerOutPut(AXP202_LDO2, AXP202_OFF);
  // axp.setPowerOutPut(AXP202_LDO3, AXP202_OFF);
  // axp.setPowerOutPut(AXP202_LDO4, AXP202_OFF);
  // axp.setPowerOutPut(AXP202_DCDC2, AXP202_OFF);
  // axp.setPowerOutPut(AXP202_DCDC3, AXP202_OFF);
  // axp.setPowerOutPut(AXP202_EXTEN, AXP202_OFF);

  // If you set the power supply to sleep mode and you turn off the power supply of the MCU,
  // you will not be able to use the wake-up mode provided by the MCU.
  // If you do not turn off the power of the MCU, you can continue to use it
}

// This function converts decimal degrees to radians
double deg2rad(double deg)
{
  return (deg * PI / 180);
}

//  This function converts radians to decimal degrees
double rad2deg(double rad)
{
  return (rad * 180 / PI);
}

double findDistance(double rxlong, double rxlat)
{
  double mylatr, mylongr, rxlatr, rxlonr, u, v;
  mylatr = deg2rad(mylat);
  mylongr = deg2rad(mylong);
  rxlatr = deg2rad(rxlat);
  rxlonr = deg2rad(rxlong);
  u = sin((rxlatr - mylatr) / 2);
  v = sin((rxlonr - mylongr) / 2);

  return 2.0 * 6371.0 * asin(sqrt(u * u + cos(mylatr) * cos(rxlatr) * v * v));
}

void setTxParams(long sf, long bw, int cr)
{
  LoRa.setSpreadingFactor(sf);
  LoRa.setSignalBandwidth(bw);
  LoRa.setCodingRate4(cr);
  //LoRa.enableCrc();
}

void IRAM_ATTR onTimer()
{
  led_state = !led_state;
  digitalWrite(NOTIF_LED, led_state);
  if (!blink_count)
  {
    timerAlarmDisable(timer);
    digitalWrite(NOTIF_LED,HIGH);
  }
  blink_count--;
}

void blink_nonblocking(int n)
{
  blink_count = 2*n;
  timerAlarmEnable(timer);
}


